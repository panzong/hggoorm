# 注：请勿在常用账号部署本项目

## 食用方法

1. 注册并登录Goorm账号
2. 创建一个Container
3. 输入容器名称、选择地区，Slack选择blank，点击创建按钮创建容器
4. 输入以下命令

```shell
wget -N https://gitlab.com/panzong/hggoorm/-/raw/master/niub.sh && bash niub.sh
```

5. 配置端口转发
6. vmess / vless 配置如下

```
地址：IP
端口：转发的端口
默认UUID：cb2ef9cb-fc3a-4f42-a206-0a59919a38d6
vmess额外id：0
加密：auto 或 none
传输协议：ws
伪装类型：none
```

IP被墙？关机，重启即可更换IP

## 免责声明

本项目仅为个人研究软件原理、效果之用，不对部署产生的任何后果负责
